<?php
function cleantext($x)
{
	//$x=str_replace("<(>&<)>","&",$x);
	//$x=str_replace("<(>,<)>",",",$x);
	$x=str_replace("\\","\\\\",$x);
	$x=str_replace("<(>","",$x);
	$x=str_replace("<)>","",$x);
	$x=str_replace("*","",$x);
	$x=str_replace("\n\n\n","\n",$x);
	$x=str_replace('{HASH}','#', $x); //add slashes
	$x=str_replace('{PERCENT}','%', $x); //add slashes
	$x=str_replace('{AMP}','&', $x); //add slashes
	$x=str_replace('{APOS}','\'', $x); //add slashes
	//$x.="			res=res.replace('%','<PERCENT>');\n";
	//$x=str_replace("","&",$x);
	return $x;
}

function cleantextjs($x)
{
	
	$x=str_replace('{HASH}','#', $x);
	$x=str_replace('{HASH}','#', $x); //add slashes
	$x=str_replace('{PERCENT}','%', $x); //add slashes
	$x=str_replace('{AMP}','\&', $x); //add slashes
	$x=str_replace('{APOS}','\\\'', $x); //add slashes
	return $x;
}
function br2nl($str)	//when text is sent back to db
{
	
	if (get_magic_quotes_gpc())
		$str=stripslashes($str);
		
	
	//$str=str_replace("<BR>","\n",$str);	//change br to nl
	//$str=str_replace("<BR />","\n",$str);	//change br to nl
	$str=str_replace("&nbsp;","", $str); //remove non-breaking space
	$str=str_replace("\\","\\\\", $str); //add slashes
	$str=str_replace("'","\'", $str); //add slashes
	$str=str_replace('"','\"', $str); //add slashes
	$str=str_replace('&','%26', $str); //add slashes
	//$str = preg_replace("/\t/", "\\t", $str);
	//$str = preg_replace("/\r?\n/", "\\n", $str);
	//echo "\n" . $str . "\n";
	return $str;
}
function execquery($sql,$table)
{
	
	global $conn; 
	connectdb();
	$x='';
	
	if ($conn->query($sql) === TRUE) {
		$x.= "Success.".mysqli_affected_rows($conn)." affected.  " ;
		$affrow=mysqli_affected_rows($conn);
	//	$x.= $table;
	//	$x.= "<br>\nAffected rows: " . mysqli_affected_rows($conn);
	} else {
		$affrow=0;

		$x.= "Error: " . $sql . "<p>" . $conn->error;
	}



	if($affrow>=1)
	{
		if($username=='System')
		{	
			
	
		}
		else{
			logsqlexec($sql,$table,$affrow,$x);
		}
		
	}
	
	return $x;
}
function cntrec($tbl,$where)
{
	global $conn; 
	//$conn1=$conn;
	connectdb();
	$sql="SELECT * FROM `$tbl` WHERE $where;";
	//echo $sql."<br>\n";
	
	$result = $conn->query($sql);
	//return $sql ."<br>". $result->num_rows;
	return $result->num_rows;
	
}


function userlog($userid,$activity)
{
	global $conn; 
	$x='';
	$username='';
	$username=$_SESSION['username'];
	$mytime="'".date("Y-m-d H:i:s")."'";
	$msql="INSERT INTO `userlog` (`times`,`userid`, `activity`) VALUES ($mytime,$userid,'$activity');";
	//$x.= $msql."<br>\n";
	$table ="Userlog";
	

	
	$x=execquery($msql,$table);

	//echo $x;
	return $x;
}

function logsqlexec($sql,$table,$affrow,$rem)
{
	global $conn; 
	$x='';
	$username='';
	$username=$_SESSION['username'];
	
	$mytime="'".date("Y-m-d H:i:s")."'";
	if($username=='System')
	{	

		$mytime="'".date("Y-m-d H:i:s",strtotime(date('Y-m-d H:i:s'))+(3600*13))."'";
		
	}
	$msql="INSERT INTO `sqllog` (`time`,`msql`, `tbl`, `affrow`, `remark`,`username`) VALUES (".$mytime.",'".br2nl($sql)."','$table',$affrow,'$rem','$username')";
	//$x.= $msql."<br>\n";

	

	
	if ($conn->query($msql) === TRUE) {
		
	//	$x.= "$table update Executed.".mysqli_affected_rows($conn)."affected.  " ;
	//	$x.= $table;
	//	$x.= "<br>\nAffected rows: " . mysqli_affected_rows($conn);
	} else {
		$x.= "Updatelog Error: " . $sql . "<br>" . $conn->error;
	}
	return $x;
}


function tbldata($valign,$border,$bgcolor,$txtcolor,$tdcontent)
{
	$x='';
	$x.="<td valign='$valign' style='background-color:$bgcolor;color:$txtcolor'>";
	$x.=$tdcontent;
	$x.="</td>";
	
	return $x;
}
function frmhead($header)
{
	$x="<form $header>";
	return $x;
}
function frmInput($itype,$iname,$iid,$ivalue,$onclick="",$onchange="",$width=20)
{
	$x="<input type='$itype' name='$iname' id='$iid' size='$width' value='$ivalue' onclick='$onclick' onchange='$onchange'>";
	return $x;
}
function hyperlink($url,$caption, $target)
{
	
	$x="<a href=\"$url\" TARGET=\"$target\">$caption</a>";
	return $x;
}
function img($src, $width, $height)
{
	$x="<img src='$src' width='$width' height='$height'>";
	return $x;
}
function frmfoot()
{
	$x="</form>";
	return $x;
}
function genRandom()
{
	$x=rand();
	//echo "<p>Random : ".$x;
	return $x;
}
function getrec($table,$field,$where, $orderby="")
{
//	echo $field;
	global $conn;
	connectdb();
	$sql="SELECT `$field` FROM $table WHERE $where $orderby;";
	$result = $conn->query($sql);
	//echo $sql."<br>";
	$row = $result->fetch_assoc();
	return $row[$field];
}
function roleact($role1,$role2='',$action,$oaction)
{
	$x='';
	
	if($_SESSION['usertype']=='admin'||$_SESSION['role']==$role1||$_SESSION['role']==$role2)
	{
		$x.=$action;
	}
	else
	{
		$x.=$oaction;
		//$x.='test';
	}
	return $x;
}

function repword($filename,$content)
{

	$x='';	
	$x.=header('Content-type: application/vnd.ms-word');
	$x.=header("Content-Disposition: attachment;Filename=$filename.doc");

	$x.= "<html>\n";
	$x.= "<meta http-equiv=\'Content-Type\' content=\'text/html; charset=Windows-1252\'>\n";
	$x.= "<body>\n";
	$x.= $content;
	$x.= "</body>";
	$x.= "</html>";
	
	
	//$myexp='yes';
	$handle = fopen($filename.".doc", "w");
    fwrite($handle, $x);
    fclose($handle);

    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename='.basename($filename.'.doc'));
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($filename.'.doc'));
    readfile($filename.'.doc');
    //echo $myexp;
	exit;
	
}

function repxl($filename,$content)
{

	$x='';	
	//$x.=header('Content-type: application/vnd.ms-excel');
	//$x.=header("Content-Disposition: attachment;Filename=$filename.xls");

//	$x.= "<html>\n";
//	$x.= "<meta http-equiv='Content-Type' content='text/html; charset=Windows-1252'>\n";
//	$x.= "<meta http-equiv='Content-Type' content='application/vnd.ms-excel'>\n";
//	$x.= "<body>\n";
	$x.= $content;
//	$x.= "</body>";
//	$x.= "</html>";
	
	
	//$myexp='yes';
	$handle = fopen($filename.".xls", "w");
    fwrite($handle, $x);
    fclose($handle);

	$x="<a href='report.meet.php?file=$filename'><img src='images/icoxl.jpg' height=20></a>";
    //echo $myexp;
	//exit;
	return $x;
}

function delfile($filename)	
{
	if (!unlink($filename))
	{
		return ("Error deleting $file");
	}
	else
	{
		
		return ("Deleted $file");
	}
}

function studyquery($select,$from,$where,$orderby,$sumfld='',$qtitle='')
{
	$header="<h1>Query Analysis</h1><p>";
	$sum=0;
	global $conn;
	connectdb();
	$sql="SELECT $select $from $table $where $orderby;";
	$header.=$sql."<br>";
	//$header.="Sumfld".$sumfld."<br>";
	echo $sql."<br>";
	if ($result=mysqli_query($conn,$sql))
	{	
		
		//if ($result->num_rows > 0) 
		//{
			// Get field information for all fields
			  $column= array();
			  $lstflds='';
			  $lstflds.="<table>";
			  $lstflds.="<tr>";
			  $lstflds.="<th>Name</th>";
			  $lstflds.="<th>Table</th>";
			  $lstflds.="<th>Type</th>";
			  $lstflds.="<th>Maxlen</th>";
			  $lstflds.="</tr>";
			  $i=0;
			  while ($fieldinfo=mysqli_fetch_field($result))
				{
					$lstflds.="<tr>";
					$lstflds.="\n<td>".$fieldinfo->name;
					$lstflds.="</td>\n<td>".$fieldinfo->table;
					$lstflds.="</td>\n<td>".$fieldinfo->type;
					$lstflds.="</td>\n<td>".$fieldinfo->max_length;
					$lstflds.="</tr>";
					$column[$i]=$fieldinfo->name;
					$dtype[$i]=$fieldinfo->type;
					$i+=1;
					
				}
				$lstflds.="</table>";
			$x.=$lstflds;
			$x.="Records Found :".$result->num_rows."<br>";
			$x.="<table border=1 cellspacing=0>";
			$x.="<tr>";
			for($j=0;$j<=$i;$j++)
			{
				$col=$column[$j];
				$datatype=$dtype[$j];
				if(!(strpos(strtoupper($col),"PASS")!==false))
				{
					$x.="<th>".strtoupper($col)."</th>";
				}
			}
			$x.="</tr>";
			
			while($row = $result->fetch_assoc()) 
			{
				$x.="<tr>";
				for($j=0;$j<=$i;$j++)
				{
					$col=$column[$j];
					$datatype=$dtype[$j];
					if(!(strpos(strtoupper($col),"PASS")!==false))
					{
						$output=$row[$col];
						
						if($datatype==252)
						{
							$output=nl2br($output);
						
						}
						elseif($datatype==12)
						{
							$output=date('d-M-Y',strtotime($output));
						}
						
						
						$x.="\n<td valign=top>".$output."</td>";
						if($sumfld==$col)
						{
							$sum+=$row[$col];
						}
					}
				}
				$x.="</tr>";
			}
			$x.="</table>";
//		}
		
			  // Free result set
			  mysqli_free_result($result);
	}
	mysqli_close($conn);
//	$x.="<p>".$lstflds;
	if($sumfld!="")
	{
		$header.="Total $sumfld : $sum<br>";
	}
//	$filename="./tmp/".$_SESSION['username']."_$qtitle_".date("Y-M-d H-i-s");
//	$content=$x;
//	$exp=repxl($filename,$content);
	//return $x;
	
//	$x=$exp."Export<br>".$header.$x;
//	$x=$header.$x;
	return $x;
}
function calcsum($tbl, $sumfld, $condition)
{
	global $conn;
	connectdb();
	$sql="SELECT SUM($sumfld) AS `SUMa` FROM $tbl $condition;";
	//	echo "<p>$sql<p>";
	$result = $conn->query($sql);
	$row = $result->fetch_assoc();
	//echo $row['SUMa'];
	return $row['SUMa'];
}
function getsqlres($select,$from,$where,$orderby)
{
		global $conn;
	connectdb();
	$sql="SELECT $select $from $table $where $orderby;";
	//echo "<p>$sql</p>";
	$header.=$sql."<br>";
	$header.="Sumfld".$sumfld."<br>";
	$x='';
	$cnt=0;
	$tmpcnt=1;
	if ($result=mysqli_query($conn,$sql))
	{	
		// Get field information for all fields
		$column= array();
		while ($fieldinfo=mysqli_fetch_field($result))
		{
			$column[]=$fieldinfo->name;	
		}
		while($row = $result->fetch_assoc()) 
		{
			$cnt+=1;
			foreach($column as $col)
			{
				if($x=='')	{ $x=";".$row[$col];}
				else		
				{
					$x.=";".$row[$col];
				}
			}
			$tmpcnt+=1;
			
		}
	}
	//echo $x;
	 $data = explode(";", $x);
	
	return $data;
}

function tbl2col($title,$data)
{
	$x="";
	$x.= "<tr>";
	$x.= "<th>$title</th>";
	$x.= "<td>$data</td>";
	$x.= "</tr>\n";
	return $x;
	
}
function update1data($table,$fld,$type,$newdata,$idfld,$id)
{
	$x="";
	if($type=='text'||$type='date')
	{
		$newdata="'$newdata'";
	}
	else
	{
		
	}
	$sql="UPDATE `$table` SET `$fld`=$newdata WHERE `$idfld`=$id";
	//$x.=$sql;
	$table="Update $table, $fld, $newdata where $idfld=$id";
	//$x.="<br>$table<br>";
	$x.="<br>".execquery($sql,$table);
	return $x;
	
}
?>
